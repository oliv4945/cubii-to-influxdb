# Cubii under-desk elliptic bike BLE to InfluxDB converter

Simple Python script to grab data from [Cubii](https://cubii.eu/) under desk elliptical bike over BLE, and save it to InfluxDB.  
I am using it as a replacement for the official smartphone application as I do not like to have to create an account and share this kind of data, and it was a fun weekend project :-)

## Installation

```Bash
python3 -m venv .venv
source .venv/bin/activate
pip install wheel
pip install -r requirements.txt
```

Copy `configuration.yaml` to `configuration.example.yaml` and edit it.  

## Test

Installation, only first time:

```Bash
pip install -r requirements.txt
```

Run tests:

```Bash
python -m pytest --cov=. --cov-config=tests/.coveragerc -vvv tests
```

## Usage

```Bash
python main.py
```
