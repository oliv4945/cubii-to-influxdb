from unittest.mock import MagicMock
import datetime
import pytest
from CUBII import CUBII

test_data_calories = [
    (1, 0.32265),
    (2, 0.32985),
    (3, 0.34515),
    (4, 0.37485),
    (5, 0.405),
    (6, 0.46485),
    (7, 0.57015),
    (8, 0.66735),
]


def test_constructor():
    cubii = CUBII("test-user", 1)
    assert cubii._user == "test-user"
    assert cubii._resistance_level == 1
    assert cubii._revolutions == 0
    assert cubii._rpm == 0


def test_get_calories_error_resistance_level():
    cubii = CUBII("test-user", 1)
    cubii._resistance_level = 123
    with pytest.raises(ValueError) as e:
        cubii.get_calories()
    assert str(e.value) == "Wrong resistance level: 123"


@pytest.mark.parametrize("resistance_level,expected_calories", test_data_calories)
def test_get_calories(resistance_level, expected_calories):
    cubii = CUBII("test-user", 1)
    assert cubii.get_calories() == 0
    cubii.revolutions = 15
    cubii.resistance_level = resistance_level
    assert cubii.get_calories() == pytest.approx(expected_calories, 1e-5)


def test_get_distance():
    cubii = CUBII("test-user", 1)
    assert cubii.get_distance() == 0
    cubii.revolutions = 15
    assert cubii.get_distance() == 0.002895


def test_property_battery():
    cubii = CUBII("test-user", 1)
    cubii.battery = 100
    assert cubii.battery == 100
    cubii.battery = 0
    assert cubii.battery == 0

    with pytest.raises(ValueError) as e:
        cubii.battery = -1
    assert str(e.value) == "Battery must be in [1:100] range, not -1"
    with pytest.raises(ValueError) as e:
        cubii.battery = 101
    assert str(e.value) == "Battery must be in [1:100] range, not 101"
    assert cubii.battery == 0


def test_property_resistance_level():
    cubii = CUBII("test-user", 1)
    cubii.resistance_level = 8
    assert cubii.resistance_level == 8
    cubii.resistance_level = 1
    assert cubii.resistance_level == 1

    with pytest.raises(ValueError) as e:
        cubii.resistance_level = 0
    assert str(e.value) == "Resistance_level must be in [1:8] range, not 0"
    with pytest.raises(ValueError) as e:
        cubii.resistance_level = 9
    assert str(e.value) == "Resistance_level must be in [1:8] range, not 9"
    assert cubii.resistance_level == 1


def test_property_revolutions():
    cubii = CUBII("test-user", 1)
    cubii.revolutions = 12345
    assert cubii.revolutions == 12345
    cubii.revolutions = 0
    assert cubii.revolutions == 0

    with pytest.raises(ValueError) as e:
        cubii.revolutions = -1
    assert str(e.value) == "Revolutions must be positive, not -1"
    assert cubii.revolutions == 0


def test_property_rpm():
    cubii = CUBII("test-user", 1)
    cubii.rpm = 12345
    assert cubii.rpm == 12345
    cubii.rpm = 0
    assert cubii.rpm == 0

    with pytest.raises(ValueError) as e:
        cubii.rpm = -1
    assert str(e.value) == "RPM must be positive, not -1"
    assert cubii.rpm == 0


@pytest.fixture
def mock_datetime_utcnow(monkeypatch):
    mock = MagicMock(wrap=datetime.datetime)
    mock.utcnow.return_value = datetime.datetime(2023, 7, 11, 20, 48, 37)
    monkeypatch.setattr(datetime, "datetime", mock)


def test_to_influxdb(mock_datetime_utcnow):
    cubii = CUBII("test-user", 1)
    cubii.rpm = 123
    cubii.revolutions = 456
    cubii.resistance_level = 7
    cubii.battery = 89
    assert (
        cubii.to_influxdb().to_line_protocol()
        == "test-user battery=89i,resistance_level=7i,revolutions=456i,rpm=123i 1689108517"
    )
