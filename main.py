import asyncio
import logging
import yaml
from typing import Union, Dict
from bleak import BleakScanner, BleakClient
from bleak.backends.device import BLEDevice
from bleak.backends.characteristic import BleakGATTCharacteristic
from CUBII import CUBII
import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
from UUIDS import UUIDS


class ExitCodes:
    DEVICE_NOT_FOUND = 1
    WRONG_CONFIGURATION = 2
    CONFIGURATION_FILE_NOT_FOUND = 3


def load_configuration(filename="configuration.yaml") -> Dict:
    try:
        with open(filename, "r", encoding="UTF-8") as file:
            configuration = yaml.load(file, Loader=yaml.Loader)
            try:
                _ = configuration["device"]
                _ = configuration["user"]
                _ = configuration["influxdb"]
                _ = configuration["influxdb"]["bucket"]
                _ = configuration["influxdb"]["organization"]
                _ = configuration["influxdb"]["token"]
                _ = configuration["influxdb"]["url"]
            except KeyError as e:
                logger.error(f"Unable to find configuration parameter: {str(e)}")
                exit(ExitCodes.WRONG_CONFIGURATION)
            return configuration
    except FileNotFoundError:
        logger.error(f"Unable to open configuration file: {filename}")
        exit(ExitCodes.CONFIGURATION_FILE_NOT_FOUND)


def set_logging_level(level=logging.WARNING):
    """Set logging level

    @param level: Logging level
    """
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(levelname)s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)


logger = logging.getLogger("Cubii-gateway")
set_logging_level(logging.DEBUG)
logger.info("Start Cubii to InfluxDB daemon")
configuration = load_configuration()
data_update = False
cubii = CUBII(
    configuration["user"],
    configuration["resistance_level"],
)
influxdb_client = influxdb_client.InfluxDBClient(
    url=configuration["influxdb"]["url"],
    token=configuration["influxdb"]["token"],
    org=configuration["influxdb"]["organization"],
)
disconnected_event = asyncio.Event()


async def send_data_to_influxdb():
    global data_update

    if data_update is True:
        data_update = False
        logger.debug(f"Send data to InfluxDB: {cubii.to_influxdb().to_line_protocol()}")
        with influxdb_client.write_api(write_options=SYNCHRONOUS) as write_api:
            a = write_api.write(
                configuration["influxdb"]["bucket"], record=cubii.to_influxdb()
            )
            cubii.revolutions = 0
    else:
        logger.debug("No new data to send to InfluxDB")


async def influxdb_timer(interval=15):
    while True:
        await asyncio.gather(
            asyncio.sleep(interval),
            send_data_to_influxdb(),
        )


def handler_battery_level(_: BleakGATTCharacteristic, data: bytearray):
    global data_update
    battery = int.from_bytes(data, byteorder="little")
    cubii.battery = battery
    data_update = True
    logger.debug(f"BLE notification - Battery: {battery} %")


def handler_resistance_level(_: BleakGATTCharacteristic, data: bytearray):
    """Not used by Cubii pro. Probably resistance level but untested
    Currently using Yaml parameter instead
    """
    global data_update
    resistance_level = int.from_bytes(data, byteorder="little")
    cubii.resistance_level = resistance_level
    data_update = True
    logger.debug(f"BLE notification - Resistance level: {resistance_level}")


def handler_revolutions(_: BleakGATTCharacteristic, data: bytearray):
    global data_update
    revolutions = int.from_bytes(data, byteorder="little")
    cubii.revolutions += revolutions
    data_update = True
    logger.debug(f"BLE notification - Revolutions: {revolutions}")


def handler_rpm(_: BleakGATTCharacteristic, data: bytearray):
    global data_update
    rpm = int.from_bytes(data, byteorder="little")
    if rpm == 0:
        cubii.rpm = 0
    else:
        # Low pass filter to slightly smooth variations between two InfluxDB write operations
        cubii.rpm = int(rpm * 0.8 + cubii.rpm * 0.2)
    data_update = True
    logger.debug(f"BLE notification - RPM: {rpm} rpm")


async def ble_search_by_name(name: str) -> Union[BLEDevice, None]:
    devices = await BleakScanner.discover(timeout=8)
    for device in devices:
        if device.details["props"].get("Name") == name:
            logger.info(f'Found device: {device.details["props"].get("Name")}')
            return device
    return None


async def wait_for_device(name: str) -> BLEDevice:
    while True:
        device = await ble_search_by_name(name)
        if device is not None:
            return device

        logger.debug("Device not found")
        await asyncio.sleep(50)


def test_disconnected_callback(client: BleakClient):
    logger.debug("Disconnected callback")
    disconnected_event.set()


async def main():
    while True:
        logger.debug(f'Search for device: {configuration["device"]}')
        device = await wait_for_device(configuration["device"])

        async with BleakClient(
            device, disconnected_callback=test_disconnected_callback
        ) as ble_client:
            logger.info(f'Connected to device {configuration["device"]}')
            await ble_client.start_notify(
                UUIDS.CHARACTERISTICS.BATTERY, handler_battery_level
            )
            # await ble_client.start_notify(
            #     UUIDS.CHARACTERISTICS.BREAK_POSITION, handler_resistance_level
            # )
            await ble_client.start_notify(
                UUIDS.CHARACTERISTICS.REVOLUTIONS, handler_revolutions
            )
            await ble_client.start_notify(UUIDS.CHARACTERISTICS.RPM, handler_rpm)

            logger.info("STARTED - Listening")
            task_influxdb_timer = asyncio.get_event_loop().create_task(influxdb_timer())

            await disconnected_event.wait()
            disconnected_event.clear()
            task_influxdb_timer.cancel()
            logger.info(f'Disconnected from device {configuration["device"]}')



try:
    asyncio.run(main())
except KeyboardInterrupt:
    logger.info("Stopped by KeyboardInterrupt")
