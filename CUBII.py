from influxdb_client import Point
from influxdb_client.domain.write_precision import WritePrecision
import datetime


class CUBII:
    def __init__(self, user: str, resistance_level: int):
        self._user = user

        self._battery: int = 0
        self._revolutions: int = 0
        self._rpm: int = 0

        self.resistance_level = resistance_level

    def get_calories(self) -> float:
        match self._resistance_level:
            case 1:
                coefficient = 0.717
            case 2:
                coefficient = 0.733
            case 3:
                coefficient = 0.767
            case 4:
                coefficient = 0.833
            case 5:
                coefficient = 0.9
            case 6:
                coefficient = 1.033
            case 7:
                coefficient = 1.267
            case 8:
                coefficient = 1.483
            case _:
                # Should not happen as the value is already check in the setter
                raise ValueError(f"Wrong resistance level: {self._resistance_level}")
        return self._revolutions * 0.03 * coefficient

    def get_distance(self) -> float:
        return self._revolutions * 1.93e-4

    def to_influxdb(self) -> Point:
        point = Point(self._user)
        point.field("battery", self._battery)
        point.field("resistance_level", self._resistance_level)
        point.field("revolutions", self._revolutions)
        point.field("rpm", self._rpm)
        point.time(datetime.datetime.utcnow(), write_precision=WritePrecision.S)
        return point

    @property
    def battery(self) -> int:
        return self._battery

    @battery.setter
    def battery(self, battery: int):
        if battery < 0 or battery > 100:
            raise ValueError(f"Battery must be in [1:100] range, not {battery}")
        else:
            self._battery = battery

    @property
    def resistance_level(self) -> int:
        return self._resistance_level

    @resistance_level.setter
    def resistance_level(self, resistance_level: int):
        if resistance_level < 1 or resistance_level > 8:
            raise ValueError(
                f"Resistance_level must be in [1:8] range, not {resistance_level}"
            )
        else:
            self._resistance_level = resistance_level

    @property
    def revolutions(self) -> int:
        return self._revolutions

    @revolutions.setter
    def revolutions(self, revolutions: int):
        if revolutions < 0:
            raise ValueError(f"Revolutions must be positive, not {revolutions}")
        else:
            self._revolutions = revolutions

    @property
    def rpm(self) -> int:
        return self._rpm

    @rpm.setter
    def rpm(self, rpm: int):
        if rpm < 0:
            raise ValueError(f"RPM must be positive, not {rpm}")
        else:
            self._rpm = rpm
